module Main where

import Lib
import Pipes
import qualified Pipes.Text as Text
import qualified Pipes.Text.IO as Text
import Pipes.Safe
import qualified System.IO as IO

main :: IO ()
main =
       runSafeT $ runEffect inAndOut
--     runSafeT $ runEffect $ producer >-> P.stdoutLn

--addString :: Monad m => Pipe String String m r
addString = forever $ do
            line  <- await
            --alllines <- lines $ yield line
            yield ("processed " ++ line)

---mapOver = map addString contents

--inAndOut :: Effect m () --IO r
inAndOut = inputFile >-> addString >-> outputFile

--inputFile :: Producer Text.Text m () --String IO r
inputFile = Text.readFile "vir_vir2vir_bil2vir" >>= yield -- forever $lift readFile  >>= yield

--outputFile :: Consumer Text.Text m () --String IO r
outputFile = Text.stdout --forever $ await >>= lift . putStrLn



--producer = do
--     readFile "vir_vir2vir_bil2vir" >-> P.take 2
--     liftIO $ putStrLn "Some long running computation"
