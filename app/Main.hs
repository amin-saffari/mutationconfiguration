{-# LANGUAGE BangPatterns, TemplateHaskell, TypeOperators #-}

import           Pipes
import           Pipes.Text.IO (fromHandle)
import           Pipes.Attoparsec (parsed)
import qualified System.IO                  as IO
import           Data.Attoparsec.Text
import           Control.Applicative
import Control.Lens
import Control.Lens.TH
import Control.Monad.State

-- Parse each configs and count it on a certain window length.
-- The order Mathematica expects is {'Ahet', 'Bhet', 'fixdiff', 'sharedhet'}
-- 'Ahet'      = a1!=a2 && b1==b2
-- 'Bhet'      = a1==a2 && b1!=b2
-- 'fixdiff'   = a1==a2 && b1==b2 && a1!=b1
-- 'sharedhet' = a1!=a2 && b1!=b2  
-- All combitions are:
-- ---------------
-- .    1/1 -> 0 0 1 1
-- .    0/0 -> 0 0 0 0
-- .    0/1 -> 0 0 1 0
-- .    1/2 -> 0 0 1 0
-- ---------------
-- 0/0  .   -> 0 0 0 0
-- 0/0  0/0 -> 0 0 0 0
-- 0/0  0/1 -> 0 0 0 1
-- 0/0  1/1 -> 0 0 1 1
-- ---------------
-- 0/1  .   -> 0 1 0 0
-- 0/1  0/0 -> !?
-- 0/1  0/1 -> 0 1 0 1
-- 0/1  1/1 -> 0 1 1 1
-- 0/1  1/2 -> 0 1 0 1
-- ---------------
-- 0/2  0/1 ->
-- 0/2  1/2 ->
-- ---------------
-- 1/1  .   -> !?
-- 1/1  0/0 -> 
-- 1/1  0/1 -> 0 0 1 0
-- 1/1  1/1
-- 1/1  1/2
-- ---------------
-- 1/2  .
-- 1/2  0/1
-- 1/2  1/1
-- 1/2  1/2
-- ---------------
-- 2/1  0/1
-- 2/1  1/1
-- 2/2  1/2
--
data Test = Test {
    a :: Int,
    b :: Int
    } deriving (Show)

testParser :: Parser Test
testParser = do
   a <- decimal
   space
   b <- decimal
   endOfLine
   return $ Test a b

type Zygousity = (Int,Int)

parserZygousity :: Parser Zygousity
parserZygousity = do 
    a<- decimal
    char '/'
    b<- decimal
    return (a,b)

parserNoSNP :: Parser Zygousity
parserNoSNP = do
    char '.'
    return (0,0)

data Config = Config {
    configuration :: !(Int,Int,Int,Int)
    } deriving (Show,Eq)

parserConfig :: Parser Config
parserConfig = do
   a <- choice [parserNoSNP,parserZygousity]
   skipSpace
   b <- choice [parserNoSNP,parserZygousity]
   endOfLine
   return $ Config (fst a, snd a, fst b, snd b)


data MutationConfig = MutationConfig {
    _ahet      :: !Int,
    _bhet      :: !Int,
    _fixdiff   :: Int,
    _sharedhet :: !Int
    } deriving (Read,Show)

makeLenses ''MutationConfig

countConfig :: MutationConfig -> Config -> MutationConfig
countConfig !s (Config (0,0,1,1)) = over fixdiff (+1) s
countConfig !s (Config (0,1,0,1)) = over sharedhet (+1) s
--countConfig :: MutationConfig -> Config -> MutationConfig 
--countConfig !s (Config (0,0,1,1)) =  modify $ over fixdiff (+1) --set fixdiff (DL.get fixdiff s + 1) s --MutationConfig $ fixdiff s + 1
--countConfig !s (Config (0,0,0,0)) = s
--countConfig !s (Config (0,0,0,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,0,1,2)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,0,1,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,1,0,0)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,1,0,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,1,1,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,1,1,2)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,2,0,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (0,2,1,2)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (1,1,0,0)) = s 
--countConfig !s (Config (1,1,0,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (1,1,1,1)) = s 
--countConfig !s (Config (1,1,1,2)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (1,2,0,0)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (1,2,0,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (1,2,1,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (1,2,1,2)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (2,1,0,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (2,1,1,1)) = DL.set fixdiff (DL.get fixdiff s + 1) s 
--countConfig !s (Config (2,2,1,2)) = DL.set fixdiff (DL.get fixdiff s + 1) s 


-- | File where the vcf combine is stored.
inputFile :: FilePath
inputFile = "test.data"--"vir_vir2vir_bil2vir"

data FooWCount = FooWCount {
    count :: !Int, -- no strict here
    _mutationConfig :: !MutationConfig
    } 

makeLenses ''FooWCount

reset :: MutationConfig -> MutationConfig
reset _ = MutationConfig 0 0 0 0

modifyFWC :: Config -> StateT FooWCount IO ()
modifyFWC cfg = do
    FooWCount c s <- get
    -- every 2 lines report me the configuration
    when (c `mod` 2 == 0 && c/=0) $ do -- when :: Monad m => Bool -> m () -> m () -- Control.Monad
        liftIO $ print s
        modify $ over mutationConfig reset 
        modify $ set mutationConfig $ MutationConfig 0 0 0 0
        put $ FooWCount c $ MutationConfig 0 0 0 0
    put $ FooWCount (c + 1) $ countConfig s cfg --equivalant to flip countConfig cfg s

-- What I don't know how to do:
-- I want to print countConfig every n lines
main :: IO ()
main = do
  rs <- IO.withFile inputFile IO.ReadMode $ \handle -> do
       let source = parsed parserConfig (fromHandle handle)
           pipe = for source $ lift . modifyFWC
--           pipe = for source $ modify .flip countConfig
       execStateT (runEffect pipe) $ FooWCount 0 (MutationConfig 0 0 0 0)
  print $ view mutationConfig  rs

--------------------------------------------
-- Before Using State
--main = IO.withFile inputFile IO.ReadMode $ \handle -> runEffect $
--      do leftover <- for (parsed parserConfig (fromHandle handle)) 
--                         (lift . print . countConfig (MutationConfig 0 0 0 0) )
--         return () -- ignore unparsed material
--
--
