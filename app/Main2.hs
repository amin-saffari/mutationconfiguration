{-# language TemplateHaskell #-}
{-# language PartialTypeSignatures #-}
{-# language Rank2Types #-}
{-# language NoMonomorphismRestriction #-}

import qualified Pipes.Prelude as P
import           Pipes
import qualified System.IO as IO
import           Control.Lens
import           Control.Lens.TH

type Zygousity = (Int,Int)

parserZygousity :: Parser Zygousity
parserZygousity = do 
    a<- decimal
    char '/'
    b<- decimal
    return (a,b)

parserNoSNP :: Parser Zygousity
parserNoSNP = do
    char '.'
    return (0,0)

data Config = Config {
    configuration :: !(Int,Int,Int,Int)
    } deriving (Show,Eq)

parserConfig :: Parser Config
parserConfig = do
   a <- choice [parserNoSNP,parserZygousity]
   skipSpace
   b <- choice [parserNoSNP,parserZygousity]
   endOfLine
   return $ Config (fst a, snd a, fst b, snd b)


data MutationConfig = MutationConfig {
    _ahet      :: !Int,
    _bhet      :: !Int,
    _fixdiff   :: Int,
    _sharedhet :: !Int
    } deriving (Read,Show)

makeLenses ''MutationConfig

countConfig :: MutationConfig -> Config -> MutationConfig
countConfig !s (Config (0,0,1,1)) = over fixdiff (+1) s
countConfig !s (Config (0,1,0,1)) = over sharedhet (+1) s

parsing :: Producer String IO () -> Producer Config IO ()
parsing 

data FooWCount = FooWCount {
    count :: !Int, -- no strict here
    _mutationConfig :: !MutationConfig
    } --deriving (Read,Show)

makeLenses ''FooWCount

reset' :: FooWCount
reset' =  FooWCount 0 $ MutationConfig 0 0 0 0


modify :: FooWCount -> Config -> IO FooWCount
modify (FooWCount c s) cfg 
    | c `mod` 20 == 0 && c/=0 = do
         print s 
         return $ FooWCount c $ MutationConfig 0 0 0 0 
    | otherwise = return $ FooWCount (c + 1) $ countConfig s cfg

-- | File where the vcf combine is stored.
inputFile :: FilePath
inputFile = "test.data"--"vir_vir2vir_bil2vir"

main = do
   rs <- IO.withFile inputFile IO.ReadMode $
           P.foldM modify (return reset') return .
           parsing . P.fromHandle 
   print $ foo rs 
