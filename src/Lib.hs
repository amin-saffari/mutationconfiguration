module Lib
    ( someFunc
--    , readFile
    ) where

--readFile :: FilePath -> Producer' String (SafeT IO) ()
--readFile file = bracket
--    (do h <- IO.openFile file IO.ReadMode
--        putStrLn $ "{" ++ file ++ " open}"
--        return h )
--    (\h -> do
--        IO.hClose h
--        putStrLn $ "{" ++ file ++ " closed}" )
--    P.fromHandle

someFunc :: IO ()
someFunc = putStrLn "someFunc"
