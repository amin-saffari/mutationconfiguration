
module Utill
      (readFile
      ) where


-- | Read the file at the given path and return as a list of lazy bytestrings.
--readFile :: FilePath -> IO [BL.ByteString]
--readFile path = verifyFilePath path >> BL.readFile path >>= 
--                    return .BL8.lines
    
-- |Check if the given path points to a real file. Stop execution and displays an error message if not.
--verifyFilePath :: FilePath -> IO ()
--verifyFilePath pth = do
--      fileExists <- doesFileExist pth
--      unless fileExists $ error ("ERROR: The file " ++ pth
--                         ++ " does not exist.")
